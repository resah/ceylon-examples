// we are normally not allowed to write a lowercase class
// (like we are not allowed to declare an uppercase attribute),
// but we could do the following:
// class \Icourtesy() { ... }
"Time to learn some courtesy"
shared class Courtesy(String salutation, String name = "Stranger", String* titles)
	extends Greeting()
	satisfies Promotable&Demotable { // whoa, we got default values
	
	shared actual variable String[] promoTitles = titles;
	
	"jeez, we got no constructors, what do we do?"
	variable Boolean posh = false;
	if (promoTitles.size>2) {
		posh = true;
	}
	// eek, this would get unreadable in realistic code examples!
	// "posh" is non-public right now, but we could share it
	// seems like I can't overwrite non-public stuff ... surprise ;)
	
	//shared actual void promote() => promoTitles = SequenceBuilder<String>().appendAll(promoTitles).append("baron").sequence;
 
 	//shared actual void demote() => promoTitles = (promoTitles.size>0 then promoTitles.spanTo(promoTitles.size-2) else []);
	
	"somewhat senseless, but just to show the for ... else"
	shared actual Boolean wasPromoted() {
		variable Boolean promoted;
		for (title in promoTitles) {
		    if (title.equals("baron")) {
		        promoted = true;
		        break;
		    }
		}
		else {
			promoted = false;
		}
		return promoted;
	}
 
	shared default Boolean vip => promoTitles.size>1 then true else false;
 
	doc("Creates proper greeting (overwrites and is overwritable)")
	shared default actual String greet() {
		variable String greeting = salutation + " ";
		for (title in promoTitles) {
			greeting += title + " ";
		}
		greeting += name;
		return greeting;
	}
	
	"creating invitation text"
	shared actual void printInvitation() {
		String greeting = greet();
		String padding = " ".repeat(47 - greeting.size);
		
		print("+--------------------------------------------------+");
		print("| " + greeting + "," + padding + " |"); 
		print("| we would be delighted to                         |");
		print("| welcome you to our party.                        |");
		if(vip) {
			print("| Please take the VIP entrance.                    |");
		}
		print("+--------------------------------------------------+\n");
	}
	
	"toString() in Java"
	shared actual String string => greet();
	
	shared actual Boolean equals(Object that) {
		if (is Courtesy that) {
			return salutation==that.salutation &&
				name==that.name &&
				promoTitles==that.promoTitles; // hm, does this actually work -> check please!
		}
		else {
			return false;
		}
	}
 
	// or we could make it really short
	hash => name.hash;

	
}