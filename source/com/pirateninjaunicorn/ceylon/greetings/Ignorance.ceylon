
shared class Ignorance()
	extends Greeting() satisfies Demotable {

	// interessting: trying to move this line to
	// bottom of class and compiler fails!
	shared actual variable String[] promoTitles = [];

	shared actual void printInvitation() {
		String greeting = greet();
		String padding = " ".repeat(36 - greeting.size);
		
		print(" +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+");
		print("/ " + greeting + "," + padding + " /"); 
		print("\\ move yer sorry ass to                 \\");
		print("/ da party tha night.                   /");
		print("+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+\n");
	}

	shared actual String greet() {
		return "What's up, dude!";
	}

	
	
}