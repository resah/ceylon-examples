doc("We could add the shared annotation here, which 
     would make the whole package public. Right now 
     only the surrounding module has access.")
package com.pirateninjaunicorn.ceylon.greetings;
